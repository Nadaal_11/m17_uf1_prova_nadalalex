using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FragateBullets : MonoBehaviour
{
    public GameObject bulletPrefab;
    private float nextActionTime = 0.0f;
    public float period = 2f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        nextActionTime += Time.deltaTime;
        if (nextActionTime > period)
        {
            nextActionTime = 0.0f;
            Spawn();
        }
    }
    void Spawn()
    {

        Instantiate(bulletPrefab, new Vector3(transform.position.x, transform.position.y, transform.position.z), Quaternion.identity);

    }
}